# Getting Started

### Get Grant Code
Follow the below to get Access Token:

* Open browser and hit the URL 127.0.0.1:8000/oauth2/authorize?response_type=code&client_id=client1&redirect_uri=http://127.0.0.1:8080/authorized
* Sign in with User credentials.
* It redirects to http://127.0.0.1:8080/authorized?code=<grant_code>
* Copy the <grant_code> and paste it in postman request 'Spring-OAuth-AccessToken' in the collection saved in this project's main folder.